#include <iostream>
#include "glm/glm.hpp"
#include "physics_units.hpp"

using namespace physics_units;

int main()
{
  std::cout << "Hello" << std::endl;



  auto speed = 200.01f*metre/second;
  auto time = 100.0f*second;
  time = second*50.0f;
  Metre<float> way = speed*time;
  std::cout << way.value << std::endl;
  std::cout << "speed: " << speed.value << std::endl;

  auto way_2 = 1302.3f*kilo*metre;
  auto time_2 = 14.7f*second;
  auto speed_2 = way_2/time_2;
  std::cout << "speed_2: " << speed_2.value << std::endl;

  auto speed_3 = -speed_2+speed;
  std::cout << "-speed_2 + speed: " << (speed_3).value << std::endl;

  speed_2 += speed;
  std::cout << "speed_2 + speed: " << (speed_2).value << std::endl;

  auto factor = 0.5f;
  speed_2 -= speed/factor;
  std::cout << "speed_2 + speed/2: " << (speed_2).value << std::endl;

  std::cout << "speed_2, -speed_3: " << (speed_2).value << ", " << (-speed_3).value << std::endl;

  auto speed_v = glm::dvec2{200.0, 15.5}*metre/second;
  time = 3.0f*second;
  Metre<glm::dvec2> l = (1.0*time)*speed_v;
  std::cout << l.value.y << std::endl;
  std::cout << "speed_v y:" << speed_v.value.y << std::endl;
  std::cout << "speed_v x:" << speed_v.value.x << std::endl;
  decltype(glm::dvec2{}*metre/second) speed_v_2 = glm::dmat2{0,1,1,0} * speed_v;
  std::cout << "speed_v_2 y:" << speed_v_2.value.y << std::endl;
  std::cout << "speed_v_2 x:" << speed_v_2.value.x << std::endl;

  std::cout << "Good Bye" << std::endl;

  return 0;
}
